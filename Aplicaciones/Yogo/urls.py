from django.urls import path
from . import views

urlpatterns=[
    path('plantilla',views.plantilla, name='plantilla'),

    path('',views.listadoClientes),
    path('listadoClientes',views.listadoClientes, name='listadoClientes'),
    path('guardarCliente/',views.guardarCliente),
    path('eliminarCliente/<idCliente>',views.eliminarCliente),
    path('editarClientes/<idCliente>',views.editarClientes),
    path('procesarActualizacionCliente/',views.procesarActualizacionCliente),

    path('listadoPuestos',views.listadoPuestos, name='listadoPuestos'),
    path('guardarPuestos/',views.guardarPuestos),
    path('eliminarPuesto/<idPuesto>',views.eliminarPuesto),
    path('editarPuestos/<idPuesto>',views.editarPuestos),
    path('procesarActualizacionPuesto/',views.procesarActualizacionPuesto),

    path('listadoEmpleados',views.listadoEmpleados, name='listadoEmpleados'),
    path('guardarEmpleado/',views.guardarEmpleado),
    path('eliminarEmpleado/<idEmpleado>',views.eliminarEmpleado),
    path('editarEmpleados/<idEmpleado>',views.editarEmpleados),
    path('procesarActualizacionEmpleado/',views.procesarActualizacionEmpleado),

    path('listadoMarcas',views.listadoMarcas, name='listadoMarcas'),
    path('guardarMarca/',views.guardarMarca),
    path('eliminarMarca/<idMarcaGE>',views.eliminarMarca),
    path('editarMarca/<idMarcaGE>',views.editarMarca),
    path('procesarActualizacionMarca/',views.procesarActualizacionMarca),

    path('listadoVehiculos',views.listadoVehiculos, name='listadoVehiculos'),
    path('guardarVehiculo/',views.guardarVehiculo),
    path('eliminarVehiculo/<idVehiculoGE>',views.eliminarVehiculo),
    path('editarVehiculo/<idVehiculoGE>',views.editarVehiculo),
    path('procesarActualizacionVehiculo/',views.procesarActualizacionVehiculo),

    path('listadoIngresos',views.listadoIngresos, name='listadoIngresos'),
    path('guardarIngreso/',views.guardarIngreso),
    path('eliminarIngreso/<idIngresoGE>',views.eliminarIngreso),
    path('editarIngreso/<idIngresoGE>',views.editarIngreso),
    path('procesarActualizacionIngreso/',views.procesarActualizacionIngreso),



]
