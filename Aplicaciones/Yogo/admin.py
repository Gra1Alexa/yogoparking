from django.contrib import admin
from .models import ClienteGE
from .models import PuestoGE
from .models import EmpleadoGE
from .models import MarcaGE
from .models import VehiculoGE
from .models import IngresoGE

# Register your models here.
admin.site.register(ClienteGE)
admin.site.register(PuestoGE)
admin.site.register(EmpleadoGE)
admin.site.register(MarcaGE)
admin.site.register(VehiculoGE)
admin.site.register(IngresoGE)
