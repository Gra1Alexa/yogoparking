from django.shortcuts import render, redirect
from .models import ClienteGE
from .models import PuestoGE
from .models import EmpleadoGE
from .models import MarcaGE
from .models import VehiculoGE
from .models import IngresoGE
# messenge
from django.contrib import messages
# Create your views here.

def plantilla(request):
    return render(request, 'plantilla.html')

def listadoClientes(request):
    return render(request,'listadoClientes.html')
# generando mensajes de confirmacion

def listadoClientes(request):
    clientesBdd=ClienteGE.objects.all()
    return render(request,'listadoClientes.html',{'clientes':clientesBdd})


def guardarCliente(request):
    #Capturando los valores del formulario por POST
    idCliente=request.POST["idCliente"]
    nombreCliente=request.POST["nombreCliente"]
    apellidoCliente=request.POST["apellidoCliente"]
    telefonoCliente=request.FILES.get("telefonoCliente")
    correoCliente=request.POST["correoCliente"]
    direccionCliente=request.POST["direccionCliente"]
    #Insertando datos mediante el ORM de DJANGO
    nuevoCliente=ClienteGE.objects.create(nombreCliente=nombreCliente,apellidoCliente=apellidoCliente,telefonoCliente=telefonoCliente,correoCliente=correoCliente,direccionCliente=direccionCliente)
    messages.success(request,'Cliente guardado exitosamente')
    return redirect('listadoClientes')
    #llamadno al ORM, Capturando


def eliminarCliente(request,id):
    clienteEliminar=ClienteGE.objects.get(idCliente=id)
    clienteEliminar.delete()
    messages.success(request,'Cliente Eliminado Exitosamente')
    return redirect('listadoClientes')

def editarClientes(request,id):
    clienteEditar=ClienteGE.objects.get(idCliente=id)
    return render(request,
    'editarClientes.html',{'cliente':clienteEditar})
    #Actualizar

def procesarActualizacionCliente(request):
    idCliente=request.POST["idCliente"]
    nombreCliente=request.POST["nombreCliente"]
    apellidoCliente=request.POST["apellidoCliente"]
    telefonoCliente=request.FILES.get("telefonoCliente")
    correoCliente=request.POST["correoCliente"]
    direccionCliente=request.POST["direccionCliente"]
    #Insertando datos mediante el ORM de DJANGO
    clienteEditar=ClienteGE.objects.get(idCliente=idCliente)
    clienteEditar.nombreCliente=nombreCliente
    clienteEditar.apellidoCliente=apellidoCliente
    clienteEditar.telefonoCliente = telefonoCliente
    clienteEditar.correoCliente=correoCliente
    clienteEditar.direccionCliente=direccionCliente

    carreraEditar.save()
    messages.success(request,
      'Cliente Actualizado Exitosamente')
    return redirect('listadoClientes')




# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
from django.contrib import messages
def listadoPuestos(request):
    return render(request,'listadoPuestos.html')
# generando mensajes de confirmacion

def listadoPuestos(request):
    puestosBdd=ClienteGE.objects.all()
    return render(request,'listadoPuestos.html',{'puestos':puestosBdd})


def guardarPuestos(request):
    #Capturando los valores del formulario por POST
    idPuesto=request.POST["idCliente"]
    nombrePuesto=request.POST["nombreCliente"]
    estadoPuesto=request.POST["apellidoCliente"]
    tipoPuesto=request.FILES.get("telefonoCliente")
    ubicacionPuesto=request.POST["correoCliente"]

    #Insertando datos mediante el ORM de DJANGO
    puestoCliente=PuestoGE.objects.create(nombrePuesto=nombrePuesto,estadoPuesto=estadoPuesto,tipoPuesto=tipoPuesto,ubicacionPuesto=ubicacionPuesto)
    messages.success(request,'Puesto guardado exitosamente')
    return redirect('listadoPuestos')
    #llamadno al ORM, Capturando


def eliminarPuesto(request,id):
    puestoEliminar=PuestoGE.objects.get(idPuesto=id)
    puestoEliminar.delete()
    messages.success(request,'Puesto Eliminado Exitosamente')
    return redirect('listadoPuestos')

def editarPuestos(request,id):
    puestoEditar=PuestoGE.objects.get(idPuesto=id)
    return render(request,
    'editarPuestos.html',{'puesto':puestoEditar})
    #Actualizar

def procesarActualizacionPuesto(request):
    idPuesto=request.POST["idPuesto"]
    nombrePuesto=request.POST["nombrePuesto"]
    estadoPuesto=request.POST["estadoPuesto"]
    tipoPuesto=request.FILES.get("tipoPuesto")
    ubicacionPuesto=request.POST["ubicacionPuesto"]
    #Insertando datos mediante el ORM de DJANGO
    puestoEditar=PuestoGE.objects.get(idPuesto=idPuesto)
    puestoEditar.nombrePuesto=nombreCliente
    puestoEditar.estadoPuesto=apellidoCliente
    puestoEditar.tipoPuesto = telefonoCliente
    puestoEditar.ubicacionPuesto=correoCliente

    carreraEditar.save()
    messages.success(request,
      'Puesto Actualizado Exitosamente')
    return redirect('listadoPuestos')


    # ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def listadoEmpleados(request):
    return render(request,'listadoEmpleados.html')
# generando mensajes de confirmacion

def listadoEmpleados(request):
    empleadosBdd=EmpleadoGE.objects.all()
    return render(request,'listadoEmpleados.html',{'empleados':empleadosBdd})


def guardarEmpleado(request):
    #Capturando los valores del formulario por POST
    idEmpleado=request.POST["idEmpleado"]
    nombreEmpleado=request.POST["nombreEmpleado"]
    apellidoEmpleado=request.POST["apellidoEmpleado"]
    telefonoEmpleado=request.FILES.get("telefonoEmpleado")
    correoEmpleado=request.POST["correoEmpleado"]
    direccionEmpleado=request.POST["direccionEmpleado"]
    #Insertando datos mediante el ORM de DJANGO
    nuevoEmpleado=EmpleadoGE.objects.create(nombreEmpleado=nombreEmpleado,apellidoEmpleado=apellidoEmpleado,telefonoEmpleado=telefonoEmpleado,correoEmpleado=correoEmpleado,direccionEmpleado=direccionEmpleado)
    messages.success(request,'Empleado guardado exitosamente')
    return redirect('listadoEmpleados')
    #llamadno al ORM, Capturando


def eliminarEmpleado(request,id):
    empleadoEliminar=EmpleadoGE.objects.get(idEmpleado=id)
    empleadoEliminar.delete()
    messages.success(request,'CEmpleado Eliminado Exitosamente')
    return redirect('listadoEmpleados')

def editarEmpleados(request,id):
    empleadoEditar=EmpleadoGE.objects.get(idEmpleado=id)
    return render(request,
    'editarEmpleados.html',{'empleado':empleadoEditar})
    #Actualizar

def procesarActualizacionEmpleado(request):
    idCliente=request.POST["idCliente"]
    nombreCliente=request.POST["nombreCliente"]
    apellidoCliente=request.POST["apellidoCliente"]
    telefonoCliente=request.FILES.get("telefonoCliente")
    correoCliente=request.POST["correoCliente"]
    direccionCliente=request.POST["direccionCliente"]
    #Insertando datos mediante el ORM de DJANGO
    empleadoEditar=EmpleadoGE.objects.get(idEmpleado=idEmpleado)
    empleadoEditar.nombreEmpleado=nombreEmpleado
    empleadoEditar.apellidoEmpleado=apellidoEmpleado
    empleadoEditar.telefonoEmpleado = telefonoEmpleado
    empleadoEditar.correoEmpleado=correoEmpleado
    empleadoEditar.direccionEmpleado=direccionEmpleado

    empleadoEditar.save()
    messages.success(request,
      'Empleado Actualizado Exitosamente')
    return redirect('listadoEmpleados')

# ------------ vista viviana ---------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------

def listadoMarcas(request):
    return render(request,'listadoMarcas.html')
# generando mensajes de confirmacion

def listadoMarcas(request):
    marcasBdd=MarcaGE.objects.all()
    return render(request,'listadoMarcas.html',{'marcas':marcasBdd})


def guardarMarca(request):
    #Capturando los valores del formulario por POST

    nombreMarcaGE=request.POST["nombreMarcaGE"]
    pais_origenMarcaGE=request.POST["pais_origenMarcaGE"]
    logoMarcaGE=request.POST["logoMarcaGE"]

    #Insertando datos mediante el ORM de DJANGO
    nuevoMarca=Marca.objects.create(nombreMarcaGE=nombreMarcaGE,pais_origenMarcaGE=pais_origenMarcaGE,logoMarcaGE=logoMarcaGE)
    messages.success(request,'Marca guardada exitosamente')
    return redirect('listadoMarcas')
    #llamadno al ORM, Capturando


def eliminarMarca(request,id):
    marcaEliminar=Marca.objects.get(idMarcaGE=idMarcaGE)
    marcaEliminar.delete()
    messages.success(request,'Marca Eliminada Exitosamente')
    return redirect('listadoMarcas')

def editarMarca(request,idMarcaGE):
    marcaEditar=Marca.objects.get(idMarcaGE=idMarcaGE)
    return render(request,
    'editarMarca.html',{'marca':marcaEditar})
    #Actualizar

def procesarActualizacionMarca(request):
    idMarcaGE=request.POST["idMarcaGE"]
    nombreMarcaGE=request.POST["nombreMarcaGE"]
    pais_origenMarcaGE=request.POST["pais_origenMarcaGE"]
    logoMarcaGE=request.POST["logoMarcaGE"]
    #Insertando datos mediante el ORM de DJANGO
    marcaGEEditar=MarcaGE.objects.get(idMarcaGE=idMarcaGE)
    marcaGEEditar.nombreMarcaGE=nombreMarcaGE
    marcaGEEditar.pais_origenMarcaGE=pais_origenMarcaGE
    marcaGEEditar.logoMarcaGE = logoMarcaGE
    marcaGEEditar.save()
    messages.success(request,
      'Marca Actualizado Exitosamente')
    return redirect('listadoMarcaGEs')

# .---------------------------------------------------------------------- vehiculo----------------------------------

def listadoVehiculos(request):
    return render(request,'listadoVehiculos.html')

def listadoVehiculos(request):
    vehiculosBdd=VehiculoGE.objects.all()
    return render(request,'listadoVehiculos.html',{'vehiculos':vehiculosBdd})


def guardarVehiculo(request):
        #Capturando los valores del formulario por POST

    modeloVehiculoGE=request.POST["modeloVehiculoGE"]
    placaVehiculoGE=request.POST["placaVehiculoGE"]
    fotoVehiculoGE=request.POST["fotoVehiculoGE"]

        #Insertando datos mediante el ORM de DJANGO
    nuevoVehiculo=Vehiculo.objects.create(modeloVehiculoGE=modeloVehiculoGE,placaVehiculoGE=placaVehiculoGE,fotoVehiculoGE=fotoVehiculoGE)
    messages.success(request,'Vehiculo guardada exitosamente')
    return redirect('listadoVehiculos')
        #llamadno al ORM, Capturando


def eliminarVehiculo(request,idVehiculoGE):
    vehiculoEliminar=Vehiculo.objects.get(idVehiculoGE=idVehiculoGE)
    vehiculoEliminar.delete()
    messages.success(request,'Vehiculo Eliminado Exitosamente')
    return redirect('listadoVehiculoGEs')

def editarVehiculo(request,idVehiculoGE):
    vehiculoGEEditar=VehiculoGE.objects.get(idVehiculoGE=idVehiculoGE)
    return render(request,
    'editarVehiculoGE.html',{'vehiculoGE':vehiculoGEEditar})
        #Actualizar

def procesarActualizacionVehiculo(request):
    idVehiculoGE=request.POST["idVehiculoGE"]
    modeloVehiculoGE=request.POST["modeloVehiculoGE"]
    placaVehiculoGE=request.POST["placaVehiculoGE"]
    fotoVehiculoGE=request.POST["fotoVehiculoGE"]
    #Insertando datos mediante el ORM de DJANGO
    vehiculoGEEditar=MarcaGE.objects.get(idVehiculoGE=idVehiculoGE)
    vehiculoGEEditar.nombreMarcaGE=nombreMarcaGE
    vehiculoGEEditar.pais_origenMarcaGE=pais_origenMarcaGE
    vehiculoGEEditar.logoMarcaGE = logoMarcaGE
    vehiculoGEEditar.save()
    messages.success(request,
      'Vehiculo Actualizado Exitosamente')
    return redirect('listadoVehiculoGEs')

# -------------------------------------ingreso ---------------------------------------------------

def listadoIngresos(request):
    return render(request,'listadoIngresoGEs.html')
# generando mensajes de confirmacion

def listadoIngresos(request):
    ingresosBdd=IngresoGE.objects.all()
    return render(request,'listadoIngresos.html',{'ingresos':ingresosBdd})


def guardarIngreso(request):
        #Capturando los valores del formulario por POST

    fechaIngresoGE=request.POST["fechaIngresoGE"]
    horaIngresoGE=request.POST["horaIngresoGE"]
    fecha_salidaIngresoGE=request.POST["fecha_salidaIngresoGE"]
    hora_salidaIngresoGE=request.POST["hora_salidaIngresoGE"]
        #Insertando datos mediante el ORM de DJANGO
    nuevoVehiculo=VehiculoGE.objects.create(modeloVehiculoGE=modeloVehiculoGE,placaVehiculoGE=placaVehiculoGE,fotoVehiculoGE=fotoVehiculoGE)
    messages.success(request,'Vehiculo guardada exitosamente')
    return redirect('listadoIngresos')
        #llamadno al ORM, Capturando


def eliminarIngreso(request,idIngresoGE):
    empleadoEliminar=MarcaGE.objects.get(idIngresoGE=idIngresoGE)
    empleadoEliminar.delete()
    messages.success(request,'Ingreso Eliminado Exitosamente')
    return redirect('listadoIngresos')

def editarIngreso(request,idIngresoGE):
    ingresoEditar=IngresoGE.objects.get(idIngresoGE=idIngresoGE)
    return render(request,
    'editarIngreso.html',{'ingreso':ingresoEditar})
        #Actualizar

def procesarActualizacionIngreso(request):
    idIngresoGE=request.POST["idIngresoGE"]
    fechaIngresoGE=request.POST["fechaIngresoGE"]
    horaIngresoGE=request.POST["horaIngresoGE"]
    fecha_salidaIngresoGE=request.POST["fecha_salidaIngresoGE"]
    hora_salidaIngresoGE=request.POST["hora_salidaIngresoGE"]
        #Insertando datos mediante el ORM de DJANGO
    ingresoGEEditar=MarcaGE.objects.get(idIngresoGE=idIngresoGE)
    ingresoGEEditar.nombreMarcaGE=nombreMarcaGE
    ingresoGEEditar.pais_origenMarcaGE=pais_origenMarcaGE
    ingresoGEEditar.logoMarcaGE = logoMarcaGE
    ingresoGEEditar.save()
    messages.success(request,
      'Marca Actualizado Exitosamente')
    return redirect('listadoIngresos')
