from django.db import models

# Create your models here.
class ClienteGE(models.Model):
    idCliente=models.AutoField(primary_key=True)
    nombreCliente=models.CharField(max_length=100)
    apellidoCliente=models.CharField(max_length=100)
    telefonoCliente=models.IntegerField()
    correoCliente=models.EmailField()
    direccionCliente=models.CharField(max_length=100)
    def _str_(self):
        fila="{0}: {1} {2} {3}"
        return fila.format(self.nombreCliente,self.apellidoCliente,self.telefonoCliente,self.correoCliente,self.direccionCliente)


class PuestoGE(models.Model):
    idPuesto=models.AutoField(primary_key=True)
    nombrePuesto=models.CharField(max_length=100)
    estadoPuesto=models.CharField(max_length=100)
    tipoPuesto=models.CharField(max_length=100)
    ubicacionPuesto=models.CharField(max_length=100)
    def _str_(self):
        fila="{0}: {1} {2} {3}"
        return fila.format(self.nombrePuesto,self.estadoPuesto,self.tipoPuesto,self.ubicacionPuesto)


class EmpleadoGE(models.Model):
    idEmpleado=models.AutoField(primary_key=True)
    nombreEmpleado=models.CharField(max_length=100)
    apellidoEmpleado=models.CharField(max_length=100)
    telefonoEmpleado=models.IntegerField()
    direccionEmpleado=models.CharField(max_length=100)
    correoEmpleado=models.CharField(max_length=100)
    hojaVidaEmpleado=models.FileField()



class MarcaGE(models.Model):
    idMarcaGE=models.AutoField(primary_key=True)
    nombreMarcaGE=models.CharField(max_length=150)
    pais_origenMarcaGE=models.CharField(max_length=150)
    logoMarcaGE=models.FileField(upload_to='marcas',null=True,blank=True)

    def _str_(self):
        fila='{0}: {1} {2}- {3}'
        return fila.format(self.idMarcaGE,self.nombreMarcaGE, self.pais_origenMarcaGE,self.logoMarcaGE)


class VehiculoGE(models.Model):
    idVehiculoGE=models.AutoField(primary_key=True)
    modeloVehiculoGE=models.CharField(max_length=150)
    placaVehiculoGE=models.CharField(max_length=150)
    fotoVehiculoGE=models.FileField(upload_to='marcas',null=True,blank=True)
    cliente=models.ForeignKey(ClienteGE, null=True, blank=True, on_delete=models.PROTECT)
    marca=models.ForeignKey(MarcaGE, null=True, blank=True, on_delete=models.PROTECT)

    def _str_(self):
        fila='{0}: {1} {2}- {3}'
        return fila.format(self.idVehiculoGE,self.modeloVehiculoGE, self.placaVehiculoGE, self.fotoMarcaGE)


class IngresoGE(models.Model):
    idIngresoGE=models.AutoField(primary_key=True)
    fechaIngresoGE=models.DateField()
    horaIngresoGE=models.CharField(max_length=150)
    fecha_salidaIngresoGE=models.DateField()
    hora_salidaIngresoGE=models.CharField(max_length=150)
    empleado=models.ForeignKey(EmpleadoGE, null=True, blank=True, on_delete=models.PROTECT)
    puesto=models.ForeignKey(PuestoGE, null=True, blank=True, on_delete=models.PROTECT)
    vehiculo=models.ForeignKey(VehiculoGE, null=True, blank=True, on_delete=models.PROTECT)


    def _str_(self):
        fila='{0}: {1} {2} - {3} {4}'
        return fila.format(self.idIngresoGE,self.fechaIngresoGE, self.horaIngresoGE, self.fecha_salidaIngresoGE, self.hora_salidaIngresoGE)
